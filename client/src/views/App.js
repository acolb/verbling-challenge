import React from 'react';

export default class App extends React.Component {
	
	constructor(props) {
		super(props);
		
		// Fill up on mock data
		this.state = {
			items: [{title: "List item 3", contents: "The quick brown fox jumps over the lazy dog.", active: false, key: 2},
					{title: "List item 2", contents: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", active: false, key: 1},
					{title: "List item 1 with a superfluously long title to demonstrate its ellipsis effect", contents: "The quick brown fox jumps over the lazy dog.", active: false, key: 0}]
		}
	};
	
	addItem(item) {
		var newItem = { title: window.prompt("Please enter a title for the new item", "List item " + (this.state.items.length+1)), 
						contents: window.prompt("Please enter the content of the item", "The quick brown fox jumps over the lazy dog."),
						active: false, key: this.state.items.length}; // Use length as item id, since items are non-removable
		if (!!newItem.title && !!newItem.contents) {			
			this.setState({
				items: [newItem].concat(this.state.items)
			});
		}
	};
	
	/* Collapses/Expands an item by its ID */
	toggle(itemKey) {
		var theItem = this.state.items.find(function (item) { return item.key == itemKey })
		
		if (typeof theItem != "undefined") {
			theItem.active = !theItem.active
			
			this.setState({
				items: this.state.items
			})
		} else console.log("Undefined toggle target with id " + itemKey)
	};
	
	collapseAll() {
		this.setState({
			items: this.state.items.map(function(item) {
				item.active = false
				return item
			})
		})
	};
	
	expandAll() {
		this.setState({
			items: this.state.items.map(function(item) {
				item.active = true
				return item
			})
		})
	};
	
	toggleAll() {
		this.setState({
			items: this.state.items.map(function(item) {
				item.active = !item.active
				return item
			})
		})
	};
	
	render() {
		return (
			<div>
				<List items={this.state.items} toggle={this.toggle.bind(this)} />
				<ListButton method={this.expandAll.bind(this)} value="Open All" />
				<ListButton method={this.collapseAll.bind(this)} value="Close All" />
				<ListButton method={this.toggleAll.bind(this)} value="Toggle All" />
				<span className="new">
					<ListButton method={this.addItem.bind(this)} value="New" />
				</span>
			</div>
		)
	};
}

	
class ListButton extends React.Component {
	render() {
		return (
			<button onClick={this.props.method.bind(this)}>{this.props.value}</button>
		)
	};
};

class List extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			query: "" // Define an empty search query
		}
	};
	
	toggle(listItem) {
		this.props.toggle(listItem)
	};
	
	search(term) {
		this.setState({
			query: term.toLowerCase()
		})
	};
	
	render() {
		var self = this
		
		// Function to check if a given text body contains the active search query (if applicable)
		var searchFunction = function (item) {
			var isEmpty = self.state.query.length == 0
			var matches = function(content) { return content.toLowerCase().indexOf(self.state.query) > -1 }
			
			return isEmpty || matches(item.title) || matches(item.contents)
		}
		
		var elementList = this.props.items.filter(searchFunction).map(function(item) {
			return (
				<Item toggle={self.toggle.bind(self)} key={item.key} index={item.key} title={item.title} contents={item.contents} active={item.active} />
			)
		})
		
		return (
			<div>
				<Search report={this.search.bind(this)} />
				<ul className="item-container">
					{elementList}
				</ul>
			</div>
		)
	};
}

class Item extends React.Component {
	
	toggle() {
		this.props.toggle(this.props.index)
	};
	
	render() {
		var itemClass = ""
		if (this.props.active) itemClass = "active"

		return (
			<li className={itemClass} onClick={this.toggle.bind(this)}>
				<span className="item-title">
					{this.props.title}
				</span>
				<div className="item-body">
					{this.props.contents}
				</div>
			</li>
		)
	};
}

class Search extends React.Component {
	update(e) {
		this.props.report(e.target.value)
	};
	render() {
		return (
			<input type="text" className="searchbar" placeholder="Search..." onChange={this.update.bind(this)}>
			</input>
		)
	};
}
